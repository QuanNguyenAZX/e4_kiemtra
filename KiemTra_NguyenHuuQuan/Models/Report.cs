﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Report
    {
        [Key]
        public int ReportID { get; set; }
        public required string ReportName { get; set; }
        public int AccountID { get; set; }
        public required Account accounts { get; set; }
        public required Transaction transactions { get; set; }
        public required Log logs { get; set; }
        public int LogID { get; set; }
        public int TransactionalID { get; set; }
        
        public DateTime ReportDate { get; set; }
    }
}
