﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }
        public required string FirstName { get; set; }
        public required string LastName { get; set; }
        public string? ContactAndAddress { get; set; }
        public required string Username { get; set; }
        public required string Password { get; set; }
    }
}
