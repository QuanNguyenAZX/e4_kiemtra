﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Account
    {
        [Key]
        public int AccountID { get; set; }
        public int CustomerID { get; set; }
        public required string AccountName { get; set; }
        public required Customer customers { get; set; }
    }
}
