﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Log
    {
        [Key]
        public int LogID { get; set; }
        public int TransactionalID { get; set; }
        public required Transaction transactions { get; set; }
        public DateOnly LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }

    }
}
