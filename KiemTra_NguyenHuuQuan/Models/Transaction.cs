﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionalID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public required string Name { get; set; }
        public required Employee employees { get; set; }
        public required Customer customers { get; set; }
    }
}
