﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra_NguyenHuuQuan.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        public required string FirstName { get; set; }
        public required string LastName { get; set; }
        public string? ContactAndAddress { get; set; }
        public required string Username { get; set; }
        public required string Password { get; set; }
    }
}
